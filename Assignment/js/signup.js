var userAccountName = new Array();
var averageAge = 0;
var list = new Array();

document.getElementById('btnSearch').onclick = searchF;

// show the average of all user
document.getElementById('showAverage').onclick = function(){
    document.getElementById("averageAge").innerHTML = "<h4>The average age of all user: " + averageAge + "</h4>";
}

// open website page to load data about the firebase
function load() {
    var database = firebase.database();
    var sumAge = 0;
    var countUser = 0;
    var id = new Date().getTime();
    document.getElementById("id").value = id;
    database.ref().child("userAccount").once('value', function(snap){
       document.getElementById("databaseData").innerHTML = "";
       var table = "<table id='userTable' class='table table-hover table-inverse'>";
       table += "<thead><tr><th hidden>#</th>";
       table += "<th>Username</th>";
       table += "<th onclick='sortTable(0);'>Name</th>";
       table += "<th onclick='sortTable(1);'>Age</th>";
       table += "</tr></thead><tbody>";
       snap.forEach(function(userAccount){
           table += "<tr><th scope='row' hidden>" + userAccount.val().id +"</th>";
           table += "<td>" + userAccount.val().username + "</td>";
           table += "<td>" + userAccount.val().name + "</td>";
           table += "<td>" + userAccount.val().age + "</td></tr>";
           userAccountName.push(userAccount.val().name);
           list.push({"id": userAccount.val().id, "username": userAccount.val().username, "name": userAccount.val().name, "age": parseInt(userAccount.val().age)});
           sumAge += parseInt(userAccount.val().age);
           countUser++;
       });
       averageAge = sumAge / countUser;
       table += "</tbody></table>";
       document.getElementById("databaseData").innerHTML = table;
    });
};

// search age function
function searchF(){
    var database = firebase.database();
    var condition = document.getElementById("condition");
    var valueOfCondition = condition.options[condition.selectedIndex].value;
    var search = document.getElementById("search").value;
    if(search != ""){
        list = [];
        database.ref().child("userAccount").once('value', function(snap){
           document.getElementById("databaseData").innerHTML = "";
           var table = "<table id='userTable' class='table table-hover table-inverse'>";
           table += "<thead><tr><th hidden>#</th>";
           table += "<th>Username</th>";
           table += "<th onclick='sortTable(0);'>Name</th>";
           table += "<th onclick='sortTable(1);'>Age</th>";
           table += "</tr></thead><tbody>";
           snap.forEach(function(userAccount){
               if(valueOfCondition == "<="){
                   if(parseInt(userAccount.val().age) <= parseInt(search)){
                       table += "<tr><th scope='row' hidden>" + userAccount.val().id +"</th>";
                       table += "<td>" + userAccount.val().username + "</td>";
                       table += "<td>" + userAccount.val().name + "</td>";
                       table += "<td>" + userAccount.val().age + "</td></tr>";
                       list.push({"id": userAccount.val().id, "username": userAccount.val().username, "name": userAccount.val().name, "age": parseInt(userAccount.val().age)});
                   }
               }else{
                   if(parseInt(userAccount.val().age) >= parseInt(search)){
                       table += "<tr><th scope='row' hidden>" + userAccount.val().id +"</th>";
                       table += "<td>" + userAccount.val().username + "</td>";
                       table += "<td>" + userAccount.val().name + "</td>";
                       table += "<td>" + userAccount.val().age + "</td></tr>";
                       list.push({"id": userAccount.val().id, "username": userAccount.val().username, "name": userAccount.val().name, "age": parseInt(userAccount.val().age)});
                   }
               }
               
           });
           table += "</tbody></table>";
           document.getElementById("databaseData").innerHTML = table;
        });
    }
}

var request = new XMLHttpRequest();

document.getElementById('create').onclick = create;

// create account and input data to firebase
function create() {
    var id = document.getElementById("id").value;
    var username = document.getElementById("username").value;
    var name = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var age = document.getElementById("age").value;
    var errorMessage = checkEmpty(username, name, email, password, age);
    if(errorMessage != ""){
        document.getElementById("message").innerHTML = errorMessage + " is Empty!!";
    }else{
        document.getElementById("message").innerHTML = "";
        var again = false;
        for(var i = 0; i < userAccountName.length; i++){
            if(username == userAccountName[i]){
                again = true;
                break;
            }
        }
        if(again == true){
            document.getElementById("againMessage").innerHTML = "This username has user to use, so need to change other name!!!";
        }else{
            document.getElementById("message").innerHTML = "";
            var account = {
                id: id,
                username: username,
                name: name,
                email: email,
                password: password,
                age: age
            };
            request.open('POST', 'https://noble-anvil-166807.firebaseio.com/userAccount/.json', true);
            request.send(JSON.stringify(account));
            location.reload();
        }
    }
}

// check empty about register user account
function checkEmpty(username, name, email, password, age){
    var message = "";
    if(username == ""){
        message = "Username";
    }
    if(name == ""){
        if(message == ""){
            message = "Name";
        }else{
            message += ", Name"
        }
    }
    if(email == ""){
        if(message == ""){
            message = "Email Address";
        }else{
            message += ", Email Address"
        }
    }
    if(password == ""){
        if(message == ""){
            message = "Password";
        }else{
            message += ", Password"
        }
    }
    if(age == ""){
        if(message == ""){
            message = "Age";
        }else{
            message += ", Age"
        }
    }
    return message;
}

// sort table function
function sortTable(type) {
  if(type == 0){
      list.sort(function(a, b){
         return ((a.name < b.name) ? -1 : ((a.name == b.name) ? 0 : 1 )); 
      });
      var table = "<table id='userTable' class='table table-hover table-inverse'>";
      table += "<thead><tr><th hidden>#</th>";
      table += "<th>Username</th>";
      table += "<th onclick='sortTable(0);'>Name</th>";
      table += "<th onclick='sortTable(1);'>Age</th>";
      table += "</tr></thead><tbody>";
      for(var i = 0; i < list.length; i++){
          table += "<tr><th scope='row' hidden>" + list[i].id +"</th><td>" + list[i].username + "</td><td>" + list[i].name + "</td><td>" + list[i].age + "</td></tr>";
      }
      table += "</table>";
      document.getElementById("databaseData").innerHTML = table;
  }else{
      list.sort(function(a, b){
         return ((a.age < b.age) ? -1 : ((a.age == b.age) ? 0 : 1 )); 
      });
      var table = "<table id='userTable' class='table table-hover table-inverse'>";
      table += "<thead><tr><th hidden>#</th>";
      table += "<th>Username</th>";
      table += "<th onclick='sortTable(0);'>Name</th>";
      table += "<th onclick='sortTable(1);'>Age</th>";
      table += "</tr></thead><tbody>";
      for(var i = 0; i < list.length; i++){
          table += "<tr><th scope='row' hidden>" + list[i].id +"</th><td>" + list[i].username + "</td><td>" + list[i].name + "</td><td>" + list[i].age + "</td></tr>";
      }
      table += "</tbody></table>";
      document.getElementById("databaseData").innerHTML = table;
  }
}
