
var request = new XMLHttpRequest();
request.open('GET', 'data/books.json', false);
request.send(null);
var data = JSON.parse(request.responseText);
console.log(data);

var books = data.books;

// var list = document.createElement('ul');
// for (var i=0; i < books.length; i++) {
// 	console.log(books[i].title);
// 	var item = document.createElement('li');
// 	item.innerHTML = books[i].title + " : " + books[i].year;
// 	list.appendChild(item);
// }
// document.body.appendChild(list);

var list = document.createElement('table');
list.setAttribute('border', '1');
var head = document.createElement('tr');
head.innerHTML = '<th>Title</th><th>Year</th>';
list.appendChild(head);
for (var i=0; i < books.length; i++) {
	console.log(books[i].title);
	var item = document.createElement('tr');
	item.innerHTML = '<td>' + books[i].title + '</td><td>' + books[i].year + '</td>';
	list.appendChild(item);
}
document.body.appendChild(list);
