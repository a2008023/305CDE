var weather = require("./weather.js");
var restify = require("restify");
var request = require("request");
var firebase = require('firebase');
var firebasejs = require("./firebase.js");
var express = require('express');
var http = require('http');
var app = express();

const server = restify.createServer({
    name: "m",
    version: '1.0.0'
});

app.use(restify.plugins.acceptParser(server.acceptable));
app.use(restify.plugins.queryParser());
app.use(restify.plugins.bodyParser());

app.set('port', process.env.PORT || 8000);
app.get('/', function(req, res) {
    res.send('Hello World');
    res.end();
});


// get weather data about searching place 
app.get('/get/:place', function(req, res) {
    var place = req.params.place;

    firebasejs.Get(place, function(message) {
        res.send({ message: message });
        res.end();
    });
});

// get weather data about all place
app.get('/get', function(req, res) {

    firebasejs.Get("", function(message) {
        res.send({ message: message });
        res.end();
    });
});

//create new weather data about new place
app.post('/post/:place', function(req, res) {
    var place = req.params.place;
    var currentFormat = place.replace(' ', '%20');



    request.get(weather.getPlace(currentFormat), (err, res2, body) => {
        var jsonObject = JSON.parse(body);
        if (jsonObject.cod == 200) {
            firebasejs.Post(place, jsonObject.weather[0]['description'], jsonObject.main['temp'], jsonObject.main['temp_min'], jsonObject.main['temp_max'], function(message) {
                //res.setHeader('Content-Type', 'application/json');
                res.send({ message: message });
                res.end();
            });
        }
        else {
            //res.setHeader('Content-Type', 'application/json');
            res.send({ message: "Add record is not successful" });
            res.end();
        }
    });
});

//delete the weather data
app.del('/delete/:place', function(req, res) {
    var place = req.params.place;

    firebasejs.Delete(place, function(message) {
        //res.setHeader('Content-Type', 'application/json');
        res.send({ message: message });
        res.end();
    });
})

//update the weather data
app.put('/put/:place', function(req, res) {
    var place = req.params.place;
    var currentFormat = place.replace(' ', '%20');


    firebasejs.Put(place, currentFormat, function(message) {
        //res.setHeader('Content-Type', 'application/json');
        res.send({ message: message });
        res.end();
    });

})

//get weather data to display html format
app.get('/desc/:place', function(req, res) {
    var place = "";
    place = req.params.place;
    firebasejs.GetDesc(place, function(message) {
        //res.setHeader("Content-Type", "text/html");
        res.write(message);
        res.end();
    });
});

//get weather data to display html format about all place
app.get('/desc', function(req, res) {
    firebasejs.GetDesc("", function(message) {
        //res.setHeader("Content-Type", "text/html");
        res.write(message);
        res.end();
    });
});

app.get('/getthreeday/:place', function(req, res) {
    var place = req.params.place;
    var currentFormat = place.replace(' ', '%20');
    request.get(weather.getFiveDay(currentFormat), (err, res2, body) => {
        var jsonObject = JSON.parse(body);
        res.send({message:jsonObject.list});
        res.end();
    });
});

app.get('/login/:email/:password', function(req, res) {
    var email = req.params.email;
    var password = req.params.password;
    firebasejs.CheckLogin(email, password, function(message) {
        res.send({ message: message });
        res.end();
    });
});

app.listen(9000, function() {
    console.log('%s listening at %s', app.name, app.url);
});

http.createServer(app).listen(app.get('port'), function() {
    console.log(app.get('port'));
});
