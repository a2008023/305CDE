//the open weather api url to get weather data about place
exports.getPlace = function(place){
    return 'http://api.openweathermap.org/data/2.5/weather?q='+place+'&appid=7aa2c9306c5fbd12673eee3c8da3c5fd&units=metric';
}

//the open weather api url to get five day of weather data about place
exports.getFiveDay = function(place){
    return 'http://api.openweathermap.org/data/2.5/forecast?q='+place+'&appid=7aa2c9306c5fbd12673eee3c8da3c5fd&units=metric';
}
