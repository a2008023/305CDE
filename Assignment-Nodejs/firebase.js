var firebase = require('firebase');
var fire =  firebase.initializeApp({
      apiKey: "AIzaSyDPbwvK3V_OcOjogRTbdmBpMJUDbA_pNRs",
      authDomain: "noble-anvil-166807.firebaseapp.com",
      databaseURL: "https://noble-anvil-166807.firebaseio.com",
      projectId: "noble-anvil-166807",
      storageBucket: "noble-anvil-166807.appspot.com",
      messagingSenderId: "183044051199"
    
});

//create weather data to firebase
exports.Post = function(place, weather, temp, temp_min, temp_max, callback){
    var todayDate = new Date();
    var todayFormat = todayDate.getFullYear() + "/" + (todayDate.getMonth()+1) + "/" + todayDate.getDate();
    fire.database().ref('/place/'+place).set({
        place : place,
        weather : weather,
        temp : temp,
        temp_min : temp_min,
        temp_max : temp_max,
        date: todayFormat
    });
    var places = new Array();
    fire.database().ref("/place/").once('value').then(function(snap){
       snap.forEach(function(place){
            places.push(place.val().place);
       });
       
       var noReocrd = false;
       for(var i = 0; i < places.length; i++){
           if(places[i] == place){
                noReocrd = false;
                break;
            }else{
                noReocrd = true;
            }
       }
       if(noReocrd == true){
           callback("Add record is not successful");
       }else{
           callback("Add record is successful")
       }
    });
}

// update weather data on the firebase
exports.Put = function(place, currentFormat, callback){
    var request = require("request");
    var weather = require("./weather.js");
    var places = new Array();
    var todayDate = new Date();
    var todayFormat = todayDate.getFullYear() + "/" + (todayDate.getMonth()+1) + "/" + todayDate.getDate();
    fire.database().ref("/place/").once('value').then(function(snap){
       snap.forEach(function(place){
            places.push(place.val().place);
       });
       
       var noReocrd = false;
       for(var i = 0; i < places.length; i++){
           if(places[i] == place){
                noReocrd = false;
                break;
            }else{
                noReocrd = true;
            }
       }
       if(noReocrd == true){
           callback("Update record is not successful");
       }else{
           request.get(weather.getPlace(currentFormat), (err,res2,body)=>{
              var jsonObject = JSON.parse(body);
              fire.database().ref('/place/').child(place).update({
                place : place,
                weather : jsonObject.weather[0]['description'],
                temp : jsonObject.main['temp'],
                temp_min : jsonObject.main['temp_min'],
                temp_max : jsonObject.main['temp_max'],
                date : todayFormat
              });
           });
           callback("Update record is successful");
       }
    });
    
}

// delete weather data on the firebase
exports.Delete = function(place, callback){
    fire.database().ref("/place/").child(place).remove();
    var places = new Array();
    var message = "";
    fire.database().ref("/place/").once('value').then(function(snap){
       snap.forEach(function(place){
            places.push(place.val().place);
       });
       
       var noReocrd = false;
       for(var i = 0; i < places.length; i++){
           if(places[i] == place){
                noReocrd = false;
                break;
            }else{
                noReocrd = true;
            }
       }
       if(noReocrd == true){
           message = "Delete record is successful";
       }else{
           message = "Delete record is not successful";
       }
       callback(message);
    });
}

//get weather data on firebase
exports.Get = function(place, callback){
    var places = new Array();
    var message = new Array();
    if(place != ""){
       fire.database().ref("/place/").once('value').then(function(snap){
           snap.forEach(function(place){
                places.push(place.val());
           });
           
           for(var i = 0; i < places.length; i++){
               if(places[i].place == place){
                   message.push({
                       place:places[i].place,
                       weather:places[i].weather,
                       temp:places[i].temp,
                       temp_min:places[i].temp_min,
                       temp_max:places[i].temp_max,
                       date : places[i].date
                   });
                   break;
               }
           }
           if(message.length == 0){
               message.push("No result");
           }
           callback(message);
       }); 
    }else{
        fire.database().ref("/place/").once('value').then(function(snap){
           snap.forEach(function(place){
                places.push(place.val());
           });
           
           for(var i = 0; i < places.length; i++){
                 message.push({
                       place:places[i].place,
                       weather:places[i].weather,
                       temp:places[i].temp,
                       temp_min:places[i].temp_min,
                       temp_max:places[i].temp_max,
                       date:places[i].date
                 });
           }
           if(message.length == 0){
               message.push("No result");
           }
           callback(message); 
        });
    }
    
}

//get weather data to display html format on the firebase
exports.GetDesc = function(place, callback){
    var places = new Array();
    var message = "";
    if(place != ""){
       fire.database().ref("/place/").once('value').then(function(snap){
           snap.forEach(function(place){
                places.push(place.val());
           });
           
           for(var i = 0; i < places.length; i++){
               if(places[i].place == place){
                   message +="<h1>Place: " + places[i].place+"</h1>";
                   message +="<b>Weather: " + places[i].weather+"<b><br />";
                   message +="<b>Template: " + places[i].temp+"<b><br />";
                   message +="<b>Template (Min): " + places[i].temp_min+"<b><br />";
                   message +="<b>Template (Max): " + places[i].temp_max+"<b><br />";
                   message +="<b>Date: " + places[i].date+"<b><br />";
                   break;
               }
           }
           if(message == ""){
               message += "<h1>No result</h1>";
           }
           callback(message);
       }); 
    }else{
        fire.database().ref("/place/").once('value').then(function(snap){
           snap.forEach(function(place){
                places.push(place.val());
           });
           
           for(var i = 0; i < places.length; i++){
                 message +="<h1>Place: " + places[i].place+"</h1>";
                 message +="<b>Weather: " + places[i].weather+"<b><br />";
                 message +="<b>Template: " + places[i].temp+"<b><br />";
                 message +="<b>Template (Min): " + places[i].temp_min+"<b><br />";
                 message +="<b>Template (Max): " + places[i].temp_max+"<b><br />";
                 message +="<b>Date: " + places[i].date+"<b><br />";
           }
           if(message == ""){
               message += "<h1>No result</h1>";
           }
           callback(message); 
        });
    }
    
}

exports.CheckLogin = function(email, password, callback) {
    var userAccount = new Array();
    fire.database().ref("/userAccount/").once('value').then(function(snap) {
        snap.forEach(function(account) {
            userAccount.push(account.val());
        });
        var correct = false;
        for (var i = 0; i < userAccount.length; i++) {
            if (userAccount[i].email == email && userAccount[i].password == password) {
                correct = true;
                break;
            }else{
                correct = false;
            }
        }
        var message = "";
        if(correct == true){
            message = "Login is successful";
        }else{
            message = "Login is not successful";
        }
        callback(message);
    });


}