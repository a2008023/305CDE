var express = require('express');
var app = express();
var http = require('http').Server(app);
var port = process.env.POST || 8080;
var session = require('express-session');
var request = require('request');
var bodyParser = require('body-parser');

var firebase = require('./js/firebase.js');

app.use(express.urlencoded({
    extended: true
}));

app.use(session({
    secret: 'abc',
    resave: true,
    saveUninitialized: true
}));

app.use(express.static(__dirname + '/css'));
app.use(express.static(__dirname + '/vendor'));
app.use(express.static(__dirname + '/js'));
app.use(express.static(__dirname + '/images'));
app.use(express.static(__dirname + '/fonts'));
app.use(express.static(__dirname + '/fonts/font-awesome-4.7.0/css'));
app.use(express.static(__dirname + 'fonts/Linearicons-Free-v1.0.0'));
app.use(express.static(__dirname + '/header'));
app.set('views', __dirname + '/');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');


// function about home page
app.get('/', function(req, res) {
    if (req.session.email) {
        var hours = (new Date()).getHours();
        //res.set('Content-Type', 'text/html');
        res.render("weather.html");
        res.end();
    }
    else {
        res.set('Content-Type', 'text/html');
        res.render("index.html", { loginmessage: "" });
        res.end();
    }
});

// go signup page
app.get('/signup', function(req, res) {
    res.set('Content-Type', 'text/html');
    res.render("signup.html");
    res.end();
});

// logout function
app.get('/logout', function(req, res) {
    req.session.destroy();
    res.set('Content-Type', 'text/html');
    res.render("index.html", { loginmessage: "" });
    res.end();
});

// login function
app.post('/', function(req, res) {

    var email = "";
    var password = "";

    email = req.body.email;
    password = req.body.password;
    firebase.CheckLogin(email, password, function(correct) {
        if (correct == false) {
            res.set('Content-Type', 'text/html');
            res.render("index.html", { loginmessage: "Email or Password is invalid!!!" });
            res.end();
        }
        else {
            req.session.email = email;
            var hours = (new Date()).getHours();
            //res.set('Content-Type', 'text/html');
            res.render("weather.html");
            res.end();
        }
    });
});


//get heroku data about the weather of a place
app.get('/getData/:place', function(req, res) {
    var place = req.params.place;

    if (place == "all") {
        request.get('https://weatherappassignment.herokuapp.com/get', (err, res2, body) => {
            var jsonObject = JSON.parse(body);
            //console.log(jsonObject.message);
            res.send(jsonObject);
            res.end();
        });
    }
    else {
        request.get('https://weatherappassignment.herokuapp.com/get/' + place, (err, res2, body) => {
            var jsonObject = JSON.parse(body);
            //console.log(jsonObject.message);
            res.send(jsonObject);
            res.end();
        });
    }
});

// load header about html page
app.get('/header', function(req, res) {
    var hours = (new Date()).getHours();
    var message = (hours > 11) ? "Good morning" : "Good afternoon";
    res.set('Content-Type', 'text/html');
    res.render("header/header.html", { email: message + ", " + req.session.email });
    res.end();
});

//create new data in heroku
app.get('/postData/:place', function(req, res) {
    var place = req.params.place;
    request.post('https://weatherappassignment.herokuapp.com/post/' + place, (err, httpResponse, body) => {
        res.send(body);
        res.end();
    });
});

//update data in heroku
app.get('/putData/:place', function(req, res) {
    var place = req.params.place;
    request.put('https://weatherappassignment.herokuapp.com/put/' + place, (err, httpResponse, body) => {
        res.send(body);
        res.end();
    });
});

//delete data in heroku
app.get('/delData/:place', function(req, res) {
    var place = req.params.place;
    request.delete('https://weatherappassignment.herokuapp.com/delete/' + place, (err, httpResponse, body) => {
        res.send(body);
        res.end();
    });
});

// get five day data in heroku
app.get('/getFiveDay/:place', function(req, res) {
    var place = req.params.place;
    request.get('https://weatherappassignment.herokuapp.com/getthreeday/' + place, (err, httpResponse, body) => {
        res.send(body);
        res.end();
    });
});

// add favorite function
app.get('/addFavouite/:place', function(req, res) {
    var place = req.params.place;
    firebase.addFavouite(req.session.email, place);
    res.end();
});

// get favorite list function
app.get('/getFavouite', function(req, res) {
    firebase.getFavouite(req.session.email, function(body) {
        res.send({ message: body });
        res.end();
    });
});

// delete favorite function
app.get('/delFavouite/:place', function(req, res) {
    var place = req.params.place;
    var correctEmail = req.session.email;
    firebase.delFavouite(correctEmail.replace('@', '').replace('.', '') + place);
    res.end();
});


http.listen(port, function() {
    console.log('Server Port : ' + port);
});
