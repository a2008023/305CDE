var url = 'https://vtc305cde-a2008023.c9users.io/';
var request = new XMLHttpRequest();
var request2 = new XMLHttpRequest();


// get heroku data about weather of all place
function load() {
    document.getElementById('fiveedatabaseData').innerHTML = "";
    request.open('GET', 'getData/all', false);
    request.onload = function() {
        var j = JSON.parse(this.responseText);
        var table = "<table id='userTable' class='table table-hover table-inverse'>";
        table += "<thead><tr><th>#</th>";
        table += "<th style='width: 10%'>Place</th>";
        table += "<th style='width: 10%'>Weather</th>";
        table += "<th style='width: 10%'>Temp</th>";
        table += "<th style='width: 10%'>Temp(Min)</th>";
        table += "<th style='width: 10%'>Temp(Max)</th>";
        table += "<th style='width: 10%'>Last Update</th>";
        table += "<th style='width: 10%'>Add Favorite</th>";
        table += "<th style='width: 10%'>Update</th>";
        table += "<th>Delete</th>";
        table += "</tr></thead><tbody>";
        for (var i = 0; i < j.message.length; i++) {
            table += "<tr><th scope='row'>" + (i + 1) + "</th>";
            table += "<td onclick='getfiveDay(\"" + j.message[i].place + "\")'>" + j.message[i].place + "</td>";
            table += "<td onclick='getfiveDay(\"" + j.message[i].place + "\")'>" + j.message[i].weather + "</td>";
            table += "<td onclick='getfiveDay(\"" + j.message[i].place + "\")'>" + j.message[i].temp + " °C</td>";
            table += "<td onclick='getfiveDay(\"" + j.message[i].place + "\")'>" + j.message[i].temp_min + " °C</td>";
            table += "<td onclick='getfiveDay(\"" + j.message[i].place + "\")'>" + j.message[i].temp_max + " °C</td>";
            table += "<td onclick='getfiveDay(\"" + j.message[i].place + "\")'>" + j.message[i].date + "</td>";
            table += "<td><button onclick='addFavouite(\"" + j.message[i].place + "\")' class=\"btn btn-default mb-2\">Add</button></td>";
            table += "<td><button onclick='updateData(\"" + j.message[i].place + "\")' class=\"btn btn-default mb-2\">Update</button></td>";
            table += "<td><button onclick='delData(\"" + j.message[i].place + "\")' class=\"btn btn-default mb-2\">Delete</button></td></tr>";
        }
        table += "</tbody></table>";
        document.getElementById('databaseData').innerHTML = table;
    }
    request.send();
}

document.getElementById('add').onclick = add;

// create new weather about new place
function add() {
    var place = "";
    place = document.getElementById('place').value;
    if (place != "" && place.indexOf(" ") != 0) {
        var currentFormat = place.replace(' ', '%20');
        var correntUrl = 'postData/' + currentFormat;
        request.open('GET', correntUrl, false);
        request.onload = function() {
            var j = JSON.parse(this.responseText);
            if (j.message == "Add record is successful") {
                swal({
                    title: j.message,
                    type: 'success'
                });
            }
            else {
                swal({
                    title: j.message,
                    type: 'error'
                });
            }
        }
        request.send();
        document.getElementById('place').value = "";
        load();
    }
    else {
        swal({
            title: 'Please input Place',
            type: 'error'
        });
    }

}

// update data about weather
function updateData(place) {
    var place2 = "";
    place2 = place;
    if (place2 != "") {
        var currentFormat = place2.replace(' ', '%20');
        var correntUrl = 'putData/' + currentFormat
        request.open('GET', correntUrl, false);
        request.onload = function() {
            var j = JSON.parse(this.responseText);
            if (j.message == "Update record is successful") {
                swal({
                    title: j.message,
                    type: 'success'
                });
            }
            else {
                swal({
                    title: j.message,
                    type: 'error'
                });
            }
        }
        request.send();
        load();
    }
    else {
        swal({
            title: 'Please input Place',
            type: 'error'
        });
    }

}


// delete data about weather
function delData(place) {
    var place2 = "";
    place2 = place;
    if (place2 != "") {
        var currentFormat = place2.replace(' ', '%20');
        var correntUrl = 'delData/' + currentFormat;
        request.open('GET', correntUrl, false);
        request.onload = function() {
            var j = JSON.parse(this.responseText);
            if (j.message == "Delete record is successful") {
                swal({
                    title: j.message,
                    type: 'success'
                });
            }
            else {
                swal({
                    title: j.message,
                    type: 'error'
                });
            }
        }
        request.send();
        load();
    }
    else {
        swal({
            title: 'Please input Place',
            type: 'error'
        });
    }

}

document.getElementById('btnSearch').onclick = search;

// search place to watch weather about function
function search() {
    document.getElementById('fiveedatabaseData').innerHTML = "";
    var search = "";
    search = document.getElementById('search').value;
    var currentFormat = search.replace(' ', '%20');
    var table = "<table id='userTable' class='table table-hover table-inverse'>";
    table += "<thead><tr><th>#</th>";
    table += "<th style='width: 10%'>Place</th>";
    table += "<th style='width: 10%'>Weather</th>";
    table += "<th style='width: 10%'>Temp</th>";
    table += "<th style='width: 10%'>Temp(Min)</th>";
    table += "<th style='width: 10%'>Temp(Max)</th>";
    table += "<th style='width: 10%'>Last Update</th>";
    table += "<th style='width: 10%'>Add Favorite</th>";
    table += "<th style='width: 10%'>Update</th>";
    table += "<th>Delete</th>";
    table += "</tr></thead><tbody>";
    if (search != "") {
        var correntUrl = 'getData/' + currentFormat;
        request.open('GET', correntUrl, true);
        request.onload = function() {
            var j = JSON.parse(this.responseText);
            if (j.message[0] == "No result") {
                document.getElementById('databaseData').innerHTML = "<h3>No result</h3>";
            }
            else {
                for (var i = 0; i < j.message.length; i++) {
                    table += "<tr><th scope='row'>" + (i + 1) + "</th>";
                    table += "<td onclick='getfiveDay(\"" + j.message[i].place + "\")'>" + j.message[i].place + "</td>";
                    table += "<td onclick='getfiveDay(\"" + j.message[i].place + "\")'>" + j.message[i].weather + "</td>";
                    table += "<td onclick='getfiveDay(\"" + j.message[i].place + "\")'>" + j.message[i].temp + " °C</td>";
                    table += "<td onclick='getfiveDay(\"" + j.message[i].place + "\")'>" + j.message[i].temp_min + " °C</td>";
                    table += "<td onclick='getfiveDay(\"" + j.message[i].place + "\")'>" + j.message[i].temp_max + " °C</td>";
                    table += "<td onclick='getfiveDay(\"" + j.message[i].place + "\")'>" + j.message[i].date + "</td>";
                    table += "<td><button onclick='addFavouite(\"" + j.message[i].place + "\")' class=\"btn btn-default mb-2\">Add</button></td>";
                    table += "<td><button onclick='updateData(\"" + j.message[i].place + "\")' class=\"btn btn-default mb-2\">Update</button></td>";
                    table += "<td><button onclick='delData(\"" + j.message[i].place + "\")' class=\"btn btn-default mb-2\">Delete</button></td></tr>";
                }
                table += "</tbody></table>";
                document.getElementById('databaseData').innerHTML = table;
            }
        }
        request.send();
    }
    else {
        var correntUrl = 'getData/all';
        request.open('GET', correntUrl, true);
        request.onload = function() {
            var j = JSON.parse(this.responseText);
            for (var i = 0; i < j.message.length; i++) {
                table += "<tr><th scope='row'>" + (i + 1) + "</th>";
                table += "<td onclick='getfiveDay(\"" + j.message[i].place + "\")'>" + j.message[i].place + "</td>";
                table += "<td onclick='getfiveDay(\"" + j.message[i].place + "\")'>" + j.message[i].weather + "</td>";
                table += "<td onclick='getfiveDay(\"" + j.message[i].place + "\")'>" + j.message[i].temp + " °C</td>";
                table += "<td onclick='getfiveDay(\"" + j.message[i].place + "\")'>" + j.message[i].temp_min + " °C</td>";
                table += "<td onclick='getfiveDay(\"" + j.message[i].place + "\")'>" + j.message[i].temp_max + " °C</td>";
                table += "<td onclick='getfiveDay(\"" + j.message[i].place + "\")'>" + j.message[i].date + "</td>";
                table += "<td><button onclick='addFavouite(\"" + j.message[i].place + "\")' class=\"btn btn-default mb-2\">Add</button></td>";
                table += "<td><button onclick='updateData(\"" + j.message[i].place + "\")' class=\"btn btn-default mb-2\">Update</button></td>";
                table += "<td><button onclick='delData(\"" + j.message[i].place + "\")' class=\"btn btn-default mb-2\">Delete</button></td></tr>";
            }
            table += "</tbody></table>";
            document.getElementById('databaseData').innerHTML = table;
        }
        request.send();
    }
}

// get five day of weather function
function getfiveDay(place) {
    document.getElementById('fiveedatabaseData').innerHTML = "";
    var currentFormat = place.replace(' ', '%20');
    var correntUrl = 'getFiveDay/' + currentFormat;
    request.open('GET', correntUrl, true);
    request.onload = function() {
        var j = JSON.parse(this.responseText);
        var table = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><h3 class=\"panel-title\"><strong>" + place + " - The Future Five day of Weather</strong></h3></div>"
        table += "<div class=\"panel-body\" style=\"max-height: 250px;overflow-x: scroll;\">";
        table += "<table id='userTable' class='table table-hover table-inverse'>";
        table += "<thead><tr><th style='width: 15%'>#</th>";
        table += "<th style='width: 15%'>Date And Time</th>";
        table += "<th style='width: 15%'>Weather</th>";
        table += "<th style='width: 15%'>Temp</th>";
        table += "<th style='width: 15%'>Temp(Min)</th>";
        table += "<th style='width: 15%'>Temp(Max)</th>";
        table += "</tr></thead><tbody>";
        var k = 0;
        for (var i = 0; i < j.message.length; i = i + 8) {
            k++;
            table += "<tr><th scope='row'>" + k + "</th>";
            table += "<td>" + j.message[i].dt_txt + "</td>";
            table += "<td>" + j.message[i].weather[0].description + "</td>";
            table += "<td>" + j.message[i].main.temp + " °C</td>";
            table += "<td>" + j.message[i].main.temp_min + " °C</td>";
            table += "<td>" + j.message[i].main.temp_max + " °C</td></tr>";
        }
        table += "</tbody></table></div>";
        document.getElementById('fiveedatabaseData').innerHTML = table;
    }
    request.send();
}

// add favorite function
function addFavouite(place) {
    var currentFormat = place.replace(' ', '%20');
    var correntUrl = 'addFavouite/' + place;
    request.open('GET', correntUrl, false);
    request.onload = function() {
        //console.log("add Favouite");
    }
    request.send();
    getFavouite();
}

// get favorite list function
function getFavouite() {
    request2.open('GET', 'getFavouite', false);
    request2.onload = function() {
        var j = JSON.parse(this.responseText);
        var table = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><h3 class=\"panel-title\"><strong>My Favorite</strong></h3></div>"
        table += "<div class=\"panel-body\" style=\"max-height: 380px;overflow-x: scroll;\">";
        table += "<table id='userTable' class='table table-hover table-inverse'>";
        table += "<thead><tr><th style='width: 10%'>#</th>";
        table += "<th style='width: 15%'>Place</th>";
        table += "<th style='width: 15%'>Delete</th>";
        table += "</tr></thead><tbody>";
        for (var i = 0; i < j.message.length; i++) {
            table += "<tr><th scope='row'>" + (i+1) + "</th>";
            table += "<td onclick='searchFavouite(\"" + j.message[i].place + "\")'>" + j.message[i].place + "</td>";
            table += "<td><button onclick='delFavouite(\"" + j.message[i].place + "\")' class=\"btn btn-default mb-2\">Delete</button></td></tr>";
        }
        table += "</tbody></table></div>";
        document.getElementById('myFavouite').innerHTML = table;
    }
    request2.send();
}

// delete favorite function
function delFavouite(place) {
    var correntUrl = 'delFavouite/' + place
    request.open('GET', correntUrl, false);
    request.onload = function() {
        //console.log("delete Favouite");
    }
    request.send();
    getFavouite();
}

// search favorite place about the weather
function searchFavouite(place) {
    document.getElementById('search').value = place;
    search();
}
