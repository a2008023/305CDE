var firebase = require('firebase');
var fire = firebase.initializeApp({
    apiKey: "AIzaSyDPbwvK3V_OcOjogRTbdmBpMJUDbA_pNRs",
    authDomain: "noble-anvil-166807.firebaseapp.com",
    databaseURL: "https://noble-anvil-166807.firebaseio.com",
    projectId: "noble-anvil-166807",
    storageBucket: "noble-anvil-166807.appspot.com",
    messagingSenderId: "183044051199"

});
//get weather data to display html format on the firebase
exports.CheckLogin = function(email, password, callback) {
    var userAccount = new Array();
    fire.database().ref("/userAccount/").once('value').then(function(snap) {
        snap.forEach(function(account) {
            userAccount.push(account.val());
        });
        var correct = false;
        for (var i = 0; i < userAccount.length; i++) {
            if (userAccount[i].email == email && userAccount[i].password == password) {
                correct = true;
                break;
            }
            else {
                correct = false;
            }
        }
        callback(correct);
    });


}

exports.addFavouite = function(email, place) {
    var correctEmail = email.replace('@', '').replace('.', '');
    fire.database().ref('/favoutie/' + correctEmail + place).set({
        email: email,
        place: place
    });
}

exports.getFavouite = function(email, callback) {
    var places = new Array();
    var message = new Array();
    fire.database().ref("/favoutie/").once('value').then(function(snap) {
        snap.forEach(function(place) {
            places.push(place.val());
        });

        for (var i = 0; i < places.length; i++) {
            if (places[i].email == email) {
                message.push({
                    //key: places[i].key,
                    email: places[i].email,
                    place: places[i].place
                });
            }
        }
        callback(message);
    });
}

exports.delFavouite = function(key) {
    fire.database().ref("/favoutie/").child(key).remove();
}
